import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

public class App {
    public static void main(String[] args) throws Exception {
        // task 1 Tạo một mảng gồm các phần tử có giá trị từ x đến y
        int x1 = 4;
        int x2 = -4;
        int y = 7;
        int[] result1 = new int[y - x1 + 1];
        for (int i = 0; i < result1.length; i++) {
            result1[i] = x1 + i;
        }
        int[] result2 = new int[y - x2 + 1];
        for (int i = 0; i < result2.length; i++) {
            result2[i] = x2 + i;
        }
        System.out.println(" Task 1:");
        System.out.println("Mang co gia tri di tu x den y: " + Arrays.toString(result1));
        System.out.println("Mang co gia tri di tu x den y: " + Arrays.toString(result2));

        // task 2: Gộp 2 mảng lại với nhau, bỏ hết các phần tử có giá trị trùng nhau.
        System.out.println("\nTask 2:");
        int[] arr1 = { 1, 2, 3 }, arr2 = { 100, 2, 1, 10 };
        int[] arr3 = { 1, 2, 3 }, arr4 = { 1, 2, 3 };

        removeDuplicateElements(arr1, arr2);
        removeDuplicateElements(arr3, arr4);

        // task 3: Đếm số lần xuất hiện của phần tử n trong mảng
        int [] arr5 = {1,2,1,4,5,1,1,3,1};
        int n5 = 1;
        int count = 0;
        for(int i =0 ; i < arr5.length; i++){
            if(arr5[i] == n5){
                count ++;
            }
        }
        System.out.println("Task 3: Số lần xuất hiện của phần tử " + n5 + " trong mảng là: " + count);

        // task 4: Tính tổng các phần tử trong mảng
        int[] array = {1,2,3,4,5,6};
        int sum = 0;
        
        for( int num : array) {
            sum = sum+num;
        }
        System.out.println("Task 4: Kết quả là: "+sum);

         // task 5: Từ một mảng cho trước, tạo một mảng gồm toàn các số chẵn

         int[] arr6 = {1,2,3,4,5,6,7,8,9,10};
        
        ArrayList<Integer> evenNumbers5 = new ArrayList<Integer>();

        for (int i = 0; i < arr6.length; i++) {
            if (arr6[i] % 2 == 0) {
                evenNumbers5.add(arr6[i]);
            }
        }
        
        System.out.println("Task 5: Mang loc ra gom cac so chan: " + evenNumbers5);

        //Task 6 Từ 2 mảng cho trước, tạo một mảng mới trong đó mỗi phần tử là tổng của 2 phần tử ở vị trí tương ứng của 2 mảng đã cho
        int[] arr7 = {1, 0, 2, 3, 4};
        int[] arr8 = {3, 5, 6, 7, 8, 13};
        int n6 = Math.max(arr7.length,arr8.length);
        
        int[] result6 = new int[n6];
        
        for (int i = 0; i < result6.length; i++) {
            int a6 = (i < arr7.length) ? arr7[i] : 0;
            int b6 = (i < arr8.length) ? arr8[i] : 0;
            result6[i] = a6 + b6;
           
        }
        System.out.println("Task 6: Gia tri cua tung phan tu la tong cua 2 gia tri moi mang cho truoc: " + Arrays.toString(result6));

        // task 7: Từ 1 mảng cho trước tạo một mảng mới gồm các phần tử của mảng cũ nhưng không bao gồm các phần tử có giá trị trùng nhau
        int[] arr9 = {1,2,3,1,5,1,4,6,3,4};
        System.out.println("Task 7: Mảng mới không có giá trị trùng nhau: " +Arrays.toString(task7(arr9)));;

        // task 8: Lấy những phần tử có giá trị không trùng nhau của 2 mảng cho trước 
        Integer[] arr10 = {1, 2, 3};
        Integer[] arr11 = {100, 2, 1, 10};
        task8(arr10,arr11);

         // task 9 Sắp xếp các phần tử theo giá trị giảm dần
         Integer[] arr12= {1,3,1,4,2,5,6};
         ArrayList<Integer> numbers = new ArrayList<>(Arrays.asList(arr12));
         numbers.sort(Comparator.reverseOrder());

         System.out.println("Task 9: Mảng sau khi sắp xếp là: "+ numbers);

         // task 10 :Đổi phần tử từ vị trí thứ x sang vị trí thứ y
        int [] arr13 = {10, 20, 30, 40, 50};
        int x = 0;
        int z = 2;

        int temp = arr13[x];
        arr13[x] = arr13[z];
        arr13[z] = temp;

        System.out.println("Task 10: Mảng sau khi thay đổi vị trí là: "+ Arrays.toString(arr13));
}
    
    // task 2: Gộp 2 mảng lại với nhau, bỏ hết các phần tử có giá trị trùng nhau.
    public static void removeDuplicateElements(int[] arr1, int [] arr2) {
       // gộp mảng

        // Bây giờ khai báo thêm một mảng mới result để lưu trữ hai mảng cần nối, với độ
        // dài bằng length
        int length = arr1.length + arr2.length;
        int[] result = new int[length];
        int pos = 0;
        // sử dụng vòng lặp for để lưu các phần tử trong mảng array1 vào mảng result
        for (int element : arr1) {
            result[pos] = element;
            pos++;
        }
        for (int element : arr2) {
            result[pos] = element;
            pos++;
        }
        Arrays.sort(result);
        System.out.println(Arrays.toString(result));
        boolean check;
        int size = 0;
        int b[] = new int[length];

        b[0] = result[0];
        size ++;

        for (int i = 0; i < length; i++) {
            check = false;
            for (int j = 0; j < size; j++) {
                if (result[i] == b[j]) {
                    check = true;
                    break;
                }
            }
            if(!check){
                b[size] = result[i];
                size++;
            }
        }
       
        for (int i = 0; i < size; i++) {
            System.out.print(b[i] + " ");
        }
        System.out.println(" ");
    }
     // task 7: Từ 1 mảng cho trước tạo một mảng mới gồm các phần tử của mảng cũ nhưng không bao gồm các phần tử có giá trị trùng nhau
     public static int [] task7(int[]arr) {
        Set<Integer> set7 = new LinkedHashSet<>();
        for (int i : arr) {
            set7.add(i);
        }
        int[] result7 = new int[set7.size()];
        int i = 0;
        for (Integer num : set7) {
            result7[i++] = num;
        }
        return result7;
     }

      // task 8: Lấy những phần tử có giá trị không trùng nhau của 2 mảng cho trước 
      public static void task8(Integer[]arr10, Integer[]arr11) {
        List<Integer> list81 = new ArrayList<Integer>(Arrays.asList(arr10));
        List<Integer> list82 = new ArrayList<>(Arrays.asList(arr11));
        List<Integer> result8 = new ArrayList<>();
        
        for (Integer i81 : list81) {
            if (!list82.contains(i81)) {
                result8.add(i81);
            }
        }
        for (Integer i82 : list82) {
            if (!list81.contains(i82)) {
                result8.add(i82);
            }
        }
        System.out.println("Task 8: Mang gom nhung phan tu khong trung nhau lay tu 2 mang cho truoc: " + result8);
      }

}